# translation of kfindpart.po to Lithuanian
#
# Ričardas Čepas <rch@richard.eu.org>, 2003.
# Donatas Glodenis <dgvirtual@akl.lt>, 2004-2005.
# Tomas Straupis <tomasstraupis@gmail.com>, 2011.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kfindpart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-11-25 00:43+0000\n"
"PO-Revision-Date: 2024-01-16 00:25+0200\n"
"Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.0.1\n"

#: kfinddlg.cpp:31
#, kde-format
msgctxt "@title:window"
msgid "Find Files/Folders"
msgstr "Ieškoti failų / aplankų"

#: kfinddlg.cpp:48 kfinddlg.cpp:202
#, kde-format
msgctxt "the application is currently idle, there is no active search"
msgid "Idle."
msgstr "Laisvas."

#. i18n as below
#: kfinddlg.cpp:138
#, kde-format
msgid "0 items found"
msgstr "nieko nerasta"

#: kfinddlg.cpp:177
#, kde-format
msgid "Searching..."
msgstr "Ieškoma..."

#: kfinddlg.cpp:204
#, kde-format
msgid "Canceled."
msgstr "Atšaukta."

#: kfinddlg.cpp:206 kfinddlg.cpp:209 kfinddlg.cpp:212
#, kde-format
msgid "Error."
msgstr "Klaida."

#: kfinddlg.cpp:207
#, kde-format
msgid "Please specify an absolute path in the \"Look in\" box."
msgstr "Prašome nurodyti absoliutų kelią „Ieškoti čia“ laukelyje."

#: kfinddlg.cpp:210
#, kde-format
msgid "Could not find the specified folder."
msgstr "Nepavyko rasti nurodyto aplanko."

#: kfinddlg.cpp:233 kfinddlg.cpp:257
#, kde-format
msgid "one item found"
msgid_plural "%1 items found"
msgstr[0] "rastas vienas objektas"
msgstr[1] "rasti %1 objektai"
msgstr[2] "rasta %1 objektų"
msgstr[3] "rastas %1 objektas"

#: kfindtreeview.cpp:43
msgid "Read-write"
msgstr "Skaitoma-rašoma"

#: kfindtreeview.cpp:44
msgid "Read-only"
msgstr "Tik skaitoma"

#: kfindtreeview.cpp:45
msgid "Write-only"
msgstr "Tik rašoma"

#: kfindtreeview.cpp:46
msgid "Inaccessible"
msgstr "Neprieinama"

#: kfindtreeview.cpp:66
#, kde-format
msgctxt "file name column"
msgid "Name"
msgstr "Pavadinimas"

#: kfindtreeview.cpp:68
#, kde-format
msgctxt "name of the containing folder"
msgid "In Subfolder"
msgstr "Poaplankyje"

#: kfindtreeview.cpp:70
#, kde-format
msgctxt "file size column"
msgid "Size"
msgstr "Dydis"

#: kfindtreeview.cpp:72
#, kde-format
msgctxt "modified date column"
msgid "Modified"
msgstr "Pakeista"

#: kfindtreeview.cpp:74
#, kde-format
msgctxt "file permissions column"
msgid "Permissions"
msgstr "Leidimai"

#: kfindtreeview.cpp:76
#, kde-format
msgctxt "first matching line of the query string in this file"
msgid "First Matching Line"
msgstr "Pirma atitinkanti eilutė"

#: kfindtreeview.cpp:359
#, kde-format
msgctxt "@action:incontextmenu"
msgid "Copy Location"
msgstr "Kopijuoti vietą"

#: kfindtreeview.cpp:360
#, kde-format
msgctxt "@info:whatsthis copy_location"
msgid "This will copy the path of the first selected item into the clipboard."
msgstr "Tai nukopijuos į iškarpinę pirmo pažymėto elemento kelią."

#: kfindtreeview.cpp:365
#, kde-format
msgid "&Open containing folder(s)"
msgstr "&Atverti vidinį aplanką(-us)"

#: kfindtreeview.cpp:369
#, kde-format
msgid "&Delete"
msgstr "&Trinti"

#: kfindtreeview.cpp:373
#, kde-format
msgid "&Move to Trash"
msgstr "&Perkelti į šiukšliadėžę"

#: kfindtreeview.cpp:513
#, kde-format
msgctxt "@title:window"
msgid "Save Results As"
msgstr "Įrašyti rezultatus kaip"

#: kfindtreeview.cpp:538
#, kde-format
msgid "Unable to save results."
msgstr "Nepavyksta išsaugoti rezultatų."

#: kfindtreeview.cpp:552
#, kde-format
msgid "KFind Results File"
msgstr "K paieškos rezultatų failas"

#: kfindtreeview.cpp:567
#, kde-format
msgctxt "%1=filename"
msgid "Results were saved to: %1"
msgstr "Rezultatai buvo įrašyti į: %1"

#: kfindtreeview.cpp:663 kftabdlg.cpp:395
#, kde-format
msgid "&Properties"
msgstr "&Savybės"

#: kftabdlg.cpp:65
#, kde-format
msgctxt "this is the label for the name textfield"
msgid "&Named:"
msgstr "&Pavadintas:"

#: kftabdlg.cpp:68
#, fuzzy, kde-format
#| msgid ""
#| "You can use wildcard matching and \";\" for separating multiple names"
msgctxt "@info:tooltip"
msgid "You can use wildcard matching and \";\" for separating multiple names"
msgstr "Jūs galite naudoti pakaitos simbolius bei „;“ kelių vardų atskyrimui"

#: kftabdlg.cpp:74
#, fuzzy, kde-format
#| msgid "Look &in:"
msgctxt "@label:textbox"
msgid "Look &in:"
msgstr "I&eškoti čia:"

#: kftabdlg.cpp:77
#, fuzzy, kde-format
#| msgid "Include &subfolders"
msgctxt "@option:check"
msgid "Include &subfolders"
msgstr "Įskaitant pa&aplankius"

#: kftabdlg.cpp:78
#, fuzzy, kde-format
#| msgid "Case s&ensitive search"
msgctxt "@option:check"
msgid "Case s&ensitive search"
msgstr "Paieška sk&iriant didžiąsias ir mažąsias raides"

#: kftabdlg.cpp:79
#, kde-format
msgid "&Browse..."
msgstr "&Naršyti..."

#: kftabdlg.cpp:80
#, fuzzy, kde-format
#| msgid "&Use files index"
msgctxt "@option:check"
msgid "&Use files index"
msgstr "Na&udoti failų indeksą"

#: kftabdlg.cpp:81
#, fuzzy, kde-format
#| msgid "Show &hidden files"
msgctxt "@option:check"
msgid "Show &hidden files"
msgstr "Rodyti &paslėptus failus"

#: kftabdlg.cpp:100
#, kde-format
msgid ""
"<qt>Enter the filename you are looking for. <br />Alternatives may be "
"separated by a semicolon \";\".<br /><br />The filename may contain the "
"following special characters:<ul><li><b>?</b> matches any single character</"
"li><li><b>*</b> matches zero or more of any characters</li><li><b>[...]</b> "
"matches any of the characters between the braces</li></ul><br />Example "
"searches:<ul><li><b>*.kwd;*.txt</b> finds all files ending with .kwd or ."
"txt</li><li><b>go[dt]</b> finds god and got</li><li><b>Hel?o</b> finds all "
"files that start with \"Hel\" and end with \"o\", having one character in "
"between</li><li><b>My Document.kwd</b> finds a file of exactly that name</"
"li></ul></qt>"
msgstr ""
"<qt>Įrašykite ieškomo failo pavadinimą. <br />Alternatyvos gali būti "
"atskirtos kabliataškiu „;“.<br /><br />Failo pavadinime gali būti šie "
"specialūs simboliai: <ul><li><b>?</b> atitinka bet kokį vieną simbolį</"
"li><li><b>*</b> atitinka nulį ar daugiau bet kokių simbolių</"
"li><li><b>[...]</b> atitinka bet kokį simbolį iš esančių laužtiniuose "
"skliaustuose</li></ul><br />Paieškos pavyzdžiai:<ul>per <li><b>*.kwd;*.txt</"
"b> ras visus failus, besibaigiančius .kwd ar .txt</li><li><b>[kt]as</b> ras "
"„kas“ ir „tas“</li><li><b>Lab?s</b> ras visus failus, prasidedančius „Lab“ "
"ir besibaigiančius „s“, ir turinčius vieną simbolį per vidurį</"
"li><li><b>Mano Dokumentas.kwd</b> ras failą tiksliai tokiu pavadinimu</li></"
"ul></qt>"

#: kftabdlg.cpp:121
#, kde-format
msgid ""
"<qt>This lets you use the files' index created by the <i>slocate</i> package "
"to speed-up the search; remember to update the index from time to time "
"(using <i>updatedb</i>).</qt>"
msgstr ""
"<qt>Kad paieška būtų greitesnė, galite naudoti failų indeksą, sukurtą "
"<i>slocate</i> paketu; nepamirškite kartas nuo karto atnaujinti indeksą "
"(naudodami komandą <i>updatedb</i>).</qt>"

#: kftabdlg.cpp:165
#, fuzzy, kde-format
#| msgid "Find all files created or &modified:"
msgctxt "@option:check"
msgid "Find all files created or &modified:"
msgstr "Ieškoti failų, sukurtų arba pa&keistų:"

#: kftabdlg.cpp:167
#, fuzzy, kde-format
#| msgid "&between"
msgctxt "@option:radio"
msgid "&between"
msgstr "&tarp"

#: kftabdlg.cpp:169
#, fuzzy, kde-format
#| msgid "and"
msgctxt "@label:textbox"
msgid "and"
msgstr "ir"

#: kftabdlg.cpp:191
#, fuzzy, kde-format
#| msgid "File &size is:"
msgctxt "@label:textbox"
msgid "File &size is:"
msgstr "Failo &dydis yra:"

#: kftabdlg.cpp:204
#, fuzzy, kde-format
#| msgid "Files owned by &user:"
msgctxt "@label:textbox"
msgid "Files owned by &user:"
msgstr "Failai, priklausantys &naudotojui:"

#: kftabdlg.cpp:209
#, fuzzy, kde-format
#| msgid "Owned by &group:"
msgctxt "@label:textbox"
msgid "Owned by &group:"
msgstr "Priklauso &grupei:"

#: kftabdlg.cpp:212
#, kde-format
msgctxt "file size isn't considered in the search"
msgid "(none)"
msgstr "(nesvarbu)"

#: kftabdlg.cpp:213
#, kde-format
msgid "At Least"
msgstr "mažiausiai"

#: kftabdlg.cpp:214
#, kde-format
msgid "At Most"
msgstr "daugiausiai"

#: kftabdlg.cpp:215
#, kde-format
msgid "Equal To"
msgstr "lygiai"

#: kftabdlg.cpp:217 kftabdlg.cpp:828
#, kde-format
msgid "Byte"
msgid_plural "Bytes"
msgstr[0] "baitas"
msgstr[1] "baitai"
msgstr[2] "baitų"
msgstr[3] "baitas"

#: kftabdlg.cpp:218
#, kde-format
msgid "KiB"
msgstr "KiB"

#: kftabdlg.cpp:219
#, kde-format
msgid "MiB"
msgstr "MiB"

#: kftabdlg.cpp:220
#, kde-format
msgid "GiB"
msgstr "GiB"

#: kftabdlg.cpp:283
#, kde-format
msgctxt "label for the file type combobox"
msgid "File &type:"
msgstr "Failo &tipas:"

#: kftabdlg.cpp:288
#, fuzzy, kde-format
#| msgid "C&ontaining text:"
msgctxt "@label:textbox"
msgid "C&ontaining text:"
msgstr "&Kuriuose yra tekstas:"

#: kftabdlg.cpp:294
#, kde-format
msgid ""
"<qt>If specified, only files that contain this text are found. Note that not "
"all file types from the list above are supported. Please refer to the "
"documentation for a list of supported file types.</qt>"
msgstr ""
"<qt>Jei įrašyta, bus rasti tik tie failai, kuriuose yra šis tekstas. "
"Atkreipkite dėmesį, kad su šia parinktimi yra palaikomi ne visi aukščiau "
"nurodyti failų tipai. Palaikomų failų tipų sąrašo ieškokite dokumentacijoje."
"</qt>"

#: kftabdlg.cpp:302
#, fuzzy, kde-format
#| msgid "Case s&ensitive"
msgctxt "@option:check"
msgid "Case s&ensitive"
msgstr "Paieška sk&iriant didžiąsias ir mažąsias raides"

#: kftabdlg.cpp:303
#, fuzzy, kde-format
#| msgid "Include &binary files"
msgctxt "@option:check"
msgid "Include &binary files"
msgstr "Įskaityti &dvejetainius failus"

#: kftabdlg.cpp:306
#, kde-format
msgid ""
"<qt>This lets you search in any type of file, even those that usually do not "
"contain text (for example program files and images).</qt>"
msgstr ""
"<qt>Galite ieškoti bet kokio tipo failuose, net jei to tipo failuose teksto "
"paprastai nebūna (pvz., programų failuose ar paveikslėliuose).</qt>"

#: kftabdlg.cpp:313
#, kde-format
msgctxt "as in search for"
msgid "fo&r:"
msgstr "&ko:"

#: kftabdlg.cpp:315
#, fuzzy, kde-format
#| msgid "Search &metainfo sections:"
msgctxt "@label:textbox"
msgid "Search &metainfo sections:"
msgstr "Ieškoti &metainfo skyrius:"

#: kftabdlg.cpp:319
#, kde-format
msgid "All Files & Folders"
msgstr "Visi failai ir aplankai"

#: kftabdlg.cpp:320
#, kde-format
msgid "Files"
msgstr "Failai"

#: kftabdlg.cpp:321
#, kde-format
msgid "Folders"
msgstr "Aplankai"

#: kftabdlg.cpp:322
#, kde-format
msgid "Symbolic Links"
msgstr "Simbolinės nuorodos"

#: kftabdlg.cpp:323
#, kde-format
msgid "Special Files (Sockets, Device Files, ...)"
msgstr "Ypatingi failai (lizdai, įrenginių failai, ...)"

#: kftabdlg.cpp:324
#, kde-format
msgid "Executable Files"
msgstr "Vykdomi failai"

#: kftabdlg.cpp:325
#, kde-format
msgid "SUID Executable Files"
msgstr "SUID vykdomi failai"

#: kftabdlg.cpp:326
#, kde-format
msgid "All Images"
msgstr "Visi paveikslėliai"

#: kftabdlg.cpp:327
#, kde-format
msgid "All Video"
msgstr "Visi video"

#: kftabdlg.cpp:328
#, kde-format
msgid "All Sounds"
msgstr "Visi garsai"

#: kftabdlg.cpp:393
#, kde-format
msgid "Name/&Location"
msgstr "Failas/&Vieta"

#: kftabdlg.cpp:394
#, kde-format
msgctxt "tab name: search by contents"
msgid "C&ontents"
msgstr "&Turinys"

#: kftabdlg.cpp:399
#, kde-format
msgid ""
"<qt>Search within files' specific comments/metainfo<br />These are some "
"examples:<br /><ul><li><b>Audio files (mp3...)</b> Search in id3 tag for a "
"title, an album</li><li><b>Images (png...)</b> Search images with a special "
"resolution, comment...</li></ul></qt>"
msgstr ""
"<qt>Ieškoti specifiniuose failų komentaruose ar meta informacijoje <br /"
">Keletas pavyzdžių:<br /><ul><li><b>Muzikiniai failai (mp3...)</b> id3 meta "
"informacijoje ieškoti dainos, albumo pavadinimo</li><li><b>Paveikslėliai "
"(png...)</b> Ieškoti paveikslėlių, turinčių nurodytą skiriamąją raišką, "
"komentarą...</li></ul></qt>"

#: kftabdlg.cpp:407
#, kde-format
msgid ""
"<qt>If specified, search only in this field<br /><ul><li><b>Audio files "
"(mp3...)</b> This can be Title, Album...</li><li><b>Images (png...)</b> "
"Search only in Resolution, Bitdepth...</li></ul></qt>"
msgstr ""
"<qt>Jei nurodyta, ieškoti tik šiame lauke<br /><ul><li><b>Muzikiniai failai "
"(mp3...)</b> Tai gali būti pavadinimas, albumas</li><li><b>Paveikslėliai "
"(png...)</b> Ieškoti tik skiriamosios raiškos, bitų gylio...</li></ul></qt>"

#: kftabdlg.cpp:548
#, kde-format
msgid "Unable to search within a period which is less than a minute."
msgstr "Nepavyksta ieškoti trumpesniame, nei viena minutė, intervale."

#: kftabdlg.cpp:559
#, kde-format
msgid "The date is not valid."
msgstr "Data neteisinga."

#: kftabdlg.cpp:561
#, kde-format
msgid "Invalid date range."
msgstr "Blogas datos intervalas."

#: kftabdlg.cpp:563
#, kde-format
msgid "Unable to search dates in the future."
msgstr "Nepavyksta ieškoti pagal ateities datas."

#: kftabdlg.cpp:635
#, kde-format
msgid "Size is too big. Set maximum size value?"
msgstr "Dydis pernelyg didelis... Nustatyti didžiausio dydžio reikšmę?"

#: kftabdlg.cpp:635
#, kde-format
msgid "Error"
msgstr "Klaida"

#: kftabdlg.cpp:635
#, kde-format
msgid "Set"
msgstr "Nustatyti"

#: kftabdlg.cpp:635
#, kde-format
msgid "Do Not Set"
msgstr "Nenustatyti"

#: kftabdlg.cpp:818
#, kde-format
msgctxt ""
"during the previous minute(s)/hour(s)/...; dynamic context 'type': 'i' "
"minutes, 'h' hours, 'd' days, 'm' months, 'y' years"
msgid "&during the previous"
msgid_plural "&during the previous"
msgstr[0] "&per praėjusią"
msgstr[1] "&per praėjusias"
msgstr[2] "&per praėjusias"
msgstr[3] "&per praėjusią"

#: kftabdlg.cpp:819
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "minutę"
msgstr[1] "minutes"
msgstr[2] "minučių"
msgstr[3] "minutę"

#: kftabdlg.cpp:820
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "hour"
msgid_plural "hours"
msgstr[0] "valandą"
msgstr[1] "valandas"
msgstr[2] "valandų"
msgstr[3] "valandą"

#: kftabdlg.cpp:821
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "day"
msgid_plural "days"
msgstr[0] "dieną"
msgstr[1] "dienas"
msgstr[2] "dienų"
msgstr[3] "dieną"

#: kftabdlg.cpp:822
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "month"
msgid_plural "months"
msgstr[0] "mėnesį"
msgstr[1] "mėnesius"
msgstr[2] "mėnesių"
msgstr[3] "mėnesį"

#: kftabdlg.cpp:823
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "year"
msgid_plural "years"
msgstr[0] "metus"
msgstr[1] "metus"
msgstr[2] "metų"
msgstr[3] "metus"

#: kquery.cpp:556
#, kde-format
msgctxt "@title:window"
msgid "Error while using locate"
msgstr "Klaida naudojant lokalę"

#: main.cpp:27
#, kde-format
msgid "KFind"
msgstr "K paieška"

#: main.cpp:28
#, kde-format
msgid "KDE file find utility"
msgstr "KDE failų paieškos pagalbinė programa"

#: main.cpp:29
#, kde-format
msgid "(c) 1998-2021, The KDE Developers"
msgstr "(c) 1998-2021, KDE programuotojai"

#: main.cpp:31
#, fuzzy, kde-format
#| msgid "Kai Uwe Broulik"
msgctxt "@info:credit"
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: main.cpp:31
#, kde-format
msgid "Current Maintainer"
msgstr "Dabartinis prižiūrėtojas"

#: main.cpp:32
#, fuzzy, kde-format
#| msgid "Eric Coquelle"
msgctxt "@info:credit"
msgid "Eric Coquelle"
msgstr "Eric Coquelle"

#: main.cpp:32
#, kde-format
msgid "Former Maintainer"
msgstr "Ankstesnis prižiūrėtojas"

#: main.cpp:33
#, fuzzy, kde-format
#| msgid "Mark W. Webb"
msgctxt "@info:credit"
msgid "Mark W. Webb"
msgstr "Mark W. Webb"

#: main.cpp:33
#, kde-format
msgid "Developer"
msgstr "Programuotojas"

#: main.cpp:34
#, fuzzy, kde-format
#| msgid "Beppe Grimaldi"
msgctxt "@info:credit"
msgid "Beppe Grimaldi"
msgstr "Beppe Grimaldi"

#: main.cpp:34
#, kde-format
msgid "UI Design & more search options"
msgstr "Naudotojo sąsaja ir daugiau paieškos parinkčių"

#: main.cpp:35
#, fuzzy, kde-format
#| msgid "Martin Hartig"
msgctxt "@info:credit"
msgid "Martin Hartig"
msgstr "Martin Hartig"

#: main.cpp:36
#, fuzzy, kde-format
#| msgid "Stephan Kulow"
msgctxt "@info:credit"
msgid "Stephan Kulow"
msgstr "Stephan Kulow"

#: main.cpp:37
#, fuzzy, kde-format
#| msgid "Mario Weilguni"
msgctxt "@info:credit"
msgid "Mario Weilguni"
msgstr "Mario Weilguni"

#: main.cpp:38
#, fuzzy, kde-format
#| msgid "Alex Zepeda"
msgctxt "@info:credit"
msgid "Alex Zepeda"
msgstr "Alex Zepeda"

#: main.cpp:39
#, fuzzy, kde-format
#| msgid "Miroslav Flídr"
msgctxt "@info:credit"
msgid "Miroslav Flídr"
msgstr "Miroslav Flídr"

#: main.cpp:40
#, fuzzy, kde-format
#| msgid "Harri Porten"
msgctxt "@info:credit"
msgid "Harri Porten"
msgstr "Harri Porten"

#: main.cpp:41
#, fuzzy, kde-format
#| msgid "Dima Rogozin"
msgctxt "@info:credit"
msgid "Dima Rogozin"
msgstr "Dima Rogozin"

#: main.cpp:42
#, fuzzy, kde-format
#| msgid "Carsten Pfeiffer"
msgctxt "@info:credit"
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: main.cpp:43
#, fuzzy, kde-format
#| msgid "Hans Petter Bieker"
msgctxt "@info:credit"
msgid "Hans Petter Bieker"
msgstr "Hans Petter Bieker"

#: main.cpp:44
#, fuzzy, kde-format
#| msgid "Waldo Bastian"
msgctxt "@info:credit"
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#: main.cpp:44
#, kde-format
msgid "UI Design"
msgstr "Naudotojo sąsaja"

#: main.cpp:45
#, fuzzy, kde-format
#| msgid "Alexander Neundorf"
msgctxt "@info:credit"
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: main.cpp:46
#, fuzzy, kde-format
#| msgid "Clarence Dang"
msgctxt "@info:credit"
msgid "Clarence Dang"
msgstr "Clarence Dang"

#: main.cpp:47
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Ričardas Čepas, Donatas Glodenis, Moo"

#: main.cpp:47
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "rch@richard.eu.org, dgvirtual@akl.lt, <>"

#: main.cpp:50
#, kde-format
msgid "Path(s) to search"
msgstr "Kelias(-iai), kur ieškoti"

#~ msgid "Regular e&xpression"
#~ msgstr "Į&prasta išraiška"

#~ msgid "&Edit..."
#~ msgstr "&Keisti..."

#~ msgid "HTML page"
#~ msgstr "HTML puslapis"

#~ msgid "Text file"
#~ msgstr "Tekstinis failas"
